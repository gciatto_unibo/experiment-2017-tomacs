# -*- coding: utf-8 -*-
"""
Created on Tue May  3 11:49:12 2016

@author: danysk
"""

#import scipy as scp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cmx
import fnmatch
import os
import math
import re
import operator
from itertools import groupby

#a = np.linspace(0, 100, num = 10000)
#plt.plot(a, np.sin(a))
#print(a)

def getClosest(val, sortedMatrix):
#    return min(matrix, key=lambda row: abs(row[0]-val))[1]
    l = len(sortedMatrix)
    if l == 1:
        return [val] + sortedMatrix[1 :]
    if l == 2:
#        if abs(sortedMatrix[0][0] - val
        return [val] + min(sortedMatrix, key=lambda row: abs(row[0]-val))[1:]
    half = int(l / 2)
    if(sortedMatrix[half][0] < val):
        return getClosest(val, sortedMatrix[-half - 1:]) #last half matrix
    return getClosest(val, sortedMatrix[: half + 1]) # first half matrix
        

def convert(samples, matrix):
    return [getClosest(t, matrix) for t in samples]

def convFloat(x, limit='inf'):
    try:
        result = float(x)
        if result == float('inf') or result == float('-inf') or result > limit:
            return float('NaN')
        return result
    except ValueError:
        return float('NaN')

def lineToArray(line):
    return [convFloat(x, limit = 10e6) for x in line.split()]

def openCsv(path):
    with open(path, 'r') as file:
        lines = filter(lambda x: re.compile('\d').match(x[0]), file.readlines())
        return [lineToArray(line) for line in lines]

def getVarValue(var, target):
    match = re.search('(?<=' + var +'-)\d+(\.\d*)?', target)
    return match.group(0)

def collectData(testvar, base = None, dir=".", ext="txt"):
    if base == None:
        base = testvar
#    return list(
#        map(openCsv,
#            filter(lambda file: fnmatch.fnmatch(file, base + '_' + '*' + testvar + '-*.' + ext), os.listdir(dir))
#        )
#    )
    allfiles = set(filter(lambda file: fnmatch.fnmatch(file, base + '_' + '*' + testvar + '-*.' + ext), os.listdir(dir)))
#    allvalues = list(map(lambda name: getVarValue(testvarm name)))
#    print("files = " + list(allfiles))
    couples = map(lambda x: (getVarValue(testvar, x), openCsv(dir + '/' + x)), allfiles) #list of (varvalue, fileContent) tuples
    couples = sorted(couples)
    grouped = groupby(couples, lambda x: x[0])
#    groups = []
#    uniquekeys = []
#    for k, g in grouped:
#        groups.append(list(g))      # Store group iterator as a list
 #       uniquekeys.append(k)
    return dict([(k, list(v[1] for v in g)) for k, g in grouped])
    
# CONFIGURE SCRIPT
directory = 'data'
floatPrecision = '{: 0.5f}'
np.set_printoptions(formatter={'float': floatPrecision.format})
seedVar = 'seed'
maxSamples = 2000

allfiles = list(filter(lambda file: fnmatch.fnmatch(file, '*_' + seedVar + '-*.txt'), os.listdir(directory)))
floatre = '[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
split = list(set(tuple(re.split('_'+seedVar+'-'+floatre, x)) for x in allfiles))
for descriptor in split:
    print(descriptor)
    unixMatch = descriptor[0] + '*' + descriptor[2]
    matchingFiles = filter(lambda f: fnmatch.fnmatch(f, unixMatch), os.listdir(directory))
#    for file in matchingFiles:
#        matrix = openCsv(directory + '/' + file)
#        if matrix[-1][2] > 100:
#            print(file + ' ' + str(matrix[-1][2])) 
    
    contents = [openCsv(directory + '/' + file) for file in matchingFiles]
    maxTime = max(map(lambda matrix: matrix[-1][0], contents))
    minTime = min(map(lambda matrix: matrix[0][0], contents))
    desiredSamples = min(maxSamples, max(map(len, contents)))
    step = float(floatPrecision.format((maxTime - minTime) / desiredSamples))
    samples = np.linspace(minTime, maxTime, desiredSamples)
    sampled = [np.matrix(convert(samples, matrix)) for matrix in contents]
    transposedSamples = np.transpose(np.matrix(samples))
    for matrix in sampled:
        matrix[:, 0] = transposedSamples
    mean = np.nanmean(sampled, axis = 0)
    error = np.nanstd(sampled, axis = 0)
    resplit = re.split('_', descriptor[0])
    baseName = directory + '/' + resplit[0]
    finalname = '_'.join(resplit[1:]) + descriptor[2]
    np.save(baseName + '-mean_' + finalname, mean)
    np.save(baseName + '-err_'  + finalname, error)