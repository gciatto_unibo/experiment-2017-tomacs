#!/bin/sh

export JAVA_HOME=/usr/lib/jvm/java-1.8.0

SHARED_DIR=/media/shared/data

if [[ ! -d $SHARED_DIR ]]; then
    mkdir -p $SHARED_DIR
fi

./gradlew assemble --console=plain --stacktrace --no-daemon

bash launch_simulation.sh $SHARED_DIR $1 $2 $3