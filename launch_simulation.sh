#!/bin/sh

TIME=$2
TIMEV=time
SMALL=$3
SMALLV=small
SEED=$4
SEEDV=seed

RES_DIR=./src/main/resources/
TEMPLATE=tomacs.template.yml
PATTERN1='s/__'$TIMEV'__/'$TIME'/g'
PATTERN2='s/__'$SMALLV'__/'$SMALL'/g'
PATTERN3='s/__'$SEEDV'__/'$SEED'/g'

SPECS='__'$TIMEV'_'$TIME'_'$SMALLV'_'$SMALL'_'$SEEDV'_'$SEED
FNAME="tomacs$SPECS.yml"

sed -e $PATTERN1 -e $PATTERN2 -e $PATTERN3 $RES_DIR$TEMPLATE > $RES_DIR$FNAME

./gradlew runtomacs -Pdest=$1 -Pspecs=$SPECS --console=plain --stacktrace --no-daemon
